# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Service'
        db.create_table('sher_service', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal('sher', ['Service'])

        # Renaming column for 'Account.service' to match new field type.
        db.rename_column('sher_account', 'service', 'service_id')
        # Changing field 'Account.service'
        db.alter_column('sher_account', 'service_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sher.Service']))

        # Adding index on 'Account', fields ['service']
        db.create_index('sher_account', ['service_id'])


    def backwards(self, orm):
        
        # Removing index on 'Account', fields ['service']
        db.delete_index('sher_account', ['service_id'])

        # Deleting model 'Service'
        db.delete_table('sher_service')

        # Renaming column for 'Account.service' to match new field type.
        db.rename_column('sher_account', 'service_id', 'service')
        # Changing field 'Account.service'
        db.alter_column('sher_account', 'service', self.gf('django.db.models.fields.CharField')(max_length=255))


    models = {
        'sher.account': {
            'Meta': {'object_name': 'Account'},
            'authsub_token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'oauth_secret': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'oauth_token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'service': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sher.Service']"}),
            'user': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        'sher.image': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Image'},
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sher.post': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Post'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '160'})
        },
        'sher.service': {
            'Meta': {'object_name': 'Service'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'sher.status': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Status'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '160'})
        },
        'sher.video': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Video'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['sher']
