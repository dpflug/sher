# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting field 'Account.name'
        db.delete_column('sher_account', 'name')

        # Deleting field 'Account.access_token'
        db.delete_column('sher_account', 'access_token')

        # Deleting field 'Account.consumer_secret'
        db.delete_column('sher_account', 'consumer_secret')

        # Deleting field 'Account.consumer_key'
        db.delete_column('sher_account', 'consumer_key')

        # Adding field 'Account.user'
        db.add_column('sher_account', 'user', self.gf('django.db.models.fields.CharField')(default=1, max_length=255), keep_default=False)

        # Adding field 'Account.oauth_token'
        db.add_column('sher_account', 'oauth_token', self.gf('django.db.models.fields.CharField')(default=1, max_length=255), keep_default=False)

        # Adding field 'Account.oauth_secret'
        db.add_column('sher_account', 'oauth_secret', self.gf('django.db.models.fields.CharField')(default=1, max_length=255), keep_default=False)


    def backwards(self, orm):
        
        # Adding field 'Account.name'
        db.add_column('sher_account', 'name', self.gf('django.db.models.fields.CharField')(default=1, max_length=255), keep_default=False)

        # Adding field 'Account.access_token'
        db.add_column('sher_account', 'access_token', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True), keep_default=False)

        # Adding field 'Account.consumer_secret'
        db.add_column('sher_account', 'consumer_secret', self.gf('django.db.models.fields.CharField')(default=1, max_length=255), keep_default=False)

        # Adding field 'Account.consumer_key'
        db.add_column('sher_account', 'consumer_key', self.gf('django.db.models.fields.CharField')(default=1, max_length=255), keep_default=False)

        # Deleting field 'Account.user'
        db.delete_column('sher_account', 'user')

        # Deleting field 'Account.oauth_token'
        db.delete_column('sher_account', 'oauth_token')

        # Deleting field 'Account.oauth_secret'
        db.delete_column('sher_account', 'oauth_secret')


    models = {
        'sher.account': {
            'Meta': {'object_name': 'Account'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'oauth_secret': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'oauth_token': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'sher.image': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Image'},
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sher.post': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Post'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '160'})
        },
        'sher.status': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Status'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '160'})
        },
        'sher.video': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Video'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['sher']
