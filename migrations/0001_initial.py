# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Status'
        db.create_table('sher_status', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('share_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('posted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=160)),
        ))
        db.send_create_signal('sher', ['Status'])

        # Adding model 'Image'
        db.create_table('sher_image', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('share_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('expire_time', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('posted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal('sher', ['Image'])

        # Adding model 'Post'
        db.create_table('sher_post', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('share_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('expire_time', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('posted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=160)),
            ('body', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('sher', ['Post'])

        # Adding model 'Video'
        db.create_table('sher_video', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('share_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('expire_time', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('posted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('video', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('sher', ['Video'])


    def backwards(self, orm):
        
        # Deleting model 'Status'
        db.delete_table('sher_status')

        # Deleting model 'Image'
        db.delete_table('sher_image')

        # Deleting model 'Post'
        db.delete_table('sher_post')

        # Deleting model 'Video'
        db.delete_table('sher_video')


    models = {
        'sher.image': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Image'},
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sher.post': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Post'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '160'})
        },
        'sher.status': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Status'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '160'})
        },
        'sher.video': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Video'},
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['sher']
