# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Video.account'
        db.add_column('sher_video', 'account', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['sher.Account']), keep_default=False)

        # Adding field 'Status.account'
        db.add_column('sher_status', 'account', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['sher.Account']), keep_default=False)

        # Adding field 'Post.account'
        db.add_column('sher_post', 'account', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['sher.Account']), keep_default=False)

        # Adding field 'Image.account'
        db.add_column('sher_image', 'account', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['sher.Account']), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Video.account'
        db.delete_column('sher_video', 'account_id')

        # Deleting field 'Status.account'
        db.delete_column('sher_status', 'account_id')

        # Deleting field 'Post.account'
        db.delete_column('sher_post', 'account_id')

        # Deleting field 'Image.account'
        db.delete_column('sher_image', 'account_id')


    models = {
        'sher.account': {
            'Meta': {'object_name': 'Account'},
            'authsub_token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'oauth_secret': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'oauth_token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'service': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sher.Service']"}),
            'user': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        'sher.image': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Image'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sher.Account']"}),
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sher.post': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Post'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sher.Account']"}),
            'body': ('django.db.models.fields.TextField', [], {}),
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '160'})
        },
        'sher.service': {
            'Meta': {'object_name': 'Service'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'sher.status': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Status'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sher.Account']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '160'})
        },
        'sher.video': {
            'Meta': {'ordering': "['share_time']", 'object_name': 'Video'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sher.Account']"}),
            'category': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'expire_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'posted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['sher']
